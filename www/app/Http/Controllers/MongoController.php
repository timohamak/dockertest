<?php

namespace App\Http\Controllers;

use App\Models\Mongo;
use Illuminate\Http\Request;

class MongoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = Mongo::all();
        return response()->json($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        print('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = new Mongo();
        $user->title = $request->input('title');
        $user->some = $request->input('some');
        $user->save();

        print_r($user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mongo  $mongo
     * @return \Illuminate\Http\Response
     */
    public function show(Mongo $mongo)
    {
        print('show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mongo  $mongo
     * @return \Illuminate\Http\Response
     */
    public function edit(Mongo $mongo)
    {
        print('edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mongo  $mongo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mongo $mongo)
    {
        print('update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mongo  $mongo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mongo $mongo)
    {
        $mongo->delete();
        return response()->json(true);
    }
}
