<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Malikzh\PhpNCANode\NCANodeClient;

class NcanodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nca = new NCANodeClient(env('NCANODE_HOST').":".env('NCANODE_PORT'));
        print_r($nca);
        return 'tset';
    }
}
