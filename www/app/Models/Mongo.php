<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class Mongo extends Model
{
    use HasFactory;

    // protected $fillable = ['title'];

    protected $connection = 'mongodb';
    protected $collection = "users";
}
